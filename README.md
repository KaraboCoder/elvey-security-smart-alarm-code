![picture](https://bitbucket.org/KaraboCoder/elvey-security-smart-alarm-code/src/master/elvey-extra-txt.png)

**ELVEY SECURITY - SMART ALARM**

The Smart Security System is a household security system that homeowners will be able to
setup by themselves.

The device is compatible with existing intruder detectors (sensors) and will be integrated with
the detectors in a similar way as the DSC boards. Initially the targeted intruder detectors are
Indoor Motion Detectors (hardwired), Smoke Detectors (hardwired), Panic Buttons and
Magnetic Contacts. This device will allow homeowners to monitor and control their home
security system online through a mobile application.
The initial prototype will use the GSM communication system, this choice was inspired by our
need to make the system user friendly. End users will not have to setup their own networks,
the system will come preconfigured and fitted with one of the South African Telecom sim
cards. We aware of the limitations that can be caused by choosing one network operator, so
for production we suggest that we use Hologram IoT sim cards that support all major network
operators in South Africa.


Because clients will not have to load the sims with airtime, this responsibility will be for Elvey
Security. With the Hologram sims, you will not have to worry about loading the airtime, a
monthly subscription will allow the sims to remain online.
Elvey Security can offer this device to users on a monthly subscription and even allow other
security companies to monitor the security systems of their clients on a cloud dashboard. The
cloud dashboard can be a separate service that is offered to security companies on a monthly
subscription.

---

**Project Overview**

The Smart Security System will be built using the Arduino MKR 1400 GSM, it will use GSM
(3G/4G) as a form of communication. End users will be able to control the system using a
mobile application. The mobile application will be available only for iOS and Android.
The Arduino MKR board operates on 3.3v and can be supplied with a usb mobile phone
charger. It also has an onboard Lithium-Ion battery charger.
End users will be able to configure zones on the mobile app, will also get a log of all the
events fired by the detectors even when the alarm is not armed.
The project will have three parts:

1. Hardware device (Security Device)
2. Mobile Application
3. Cloud platform that will route messages between the device and the app.

---

**Software**

We decided to use open source software and hardware to reduce the costs of developing the
device. All the prototyping software packages are free.
Embedded Software:
The embedded software will be written in C++ on the Arduino platform.
Mobile Application:
Software for the will be written in JavaScript and will support only iOS and Android platforms.

---

**Hardware**

The system will be powered by the Arduino MKR 1400 GSM.